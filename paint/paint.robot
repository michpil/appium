*** Settings ***
Documentation  Paint Automation
Library  AppiumLibrary
Library  helpers.py


*** Variables ***
${DEVICE_NAME}                emulator-5554
${ANDROID_PLATFORM_NAME}      Android
${ANDROID_PLATFORM_VERSION}   11.0
${ANDROID_APP}                C:\\Users\\michp\\apks\\Draw.apk
${SERVER}                     http://localhost:4723/wd/hub
#${APP_ACT}                    .MainActivity


*** Test Cases ***
Paint Test
  [Setup]  Test Startup
#  [Teardown]  Test Shutdown

  log  ok

  swipe  100  100  50  50

  Wait until page contains element  com.simplemobiletools.draw:id/clear
  Click element  com.simplemobiletools.draw:id/clear


#  Select Ads
#  Continue to Game
#  Select Practice
#  Select Ski Jump
#  Start
#  Jump
#  Fly
#  Telemark No

*** Keywords ***
Test Startup
  clean screenshots

  Open Application  ${SERVER}
  ...  deviceName=${DEVICE_NAME}
  ...  platformName=${ANDROID_PLATFORM_NAME}
  ...  platformVersion=${ANDROID_PLATFORM_VERSION}
  ...  app=${ANDROID_APP}
#  ...  appActivity=${APP_ACT}

#  ${sid}=  get appium sessionid
#  ${sid1}=  connect driver to session  ${sid}
#  log  ${sid}
#  log  ${sid1}
#
#  connect to device rotation



Test Shutdown
  close application

#  close session
