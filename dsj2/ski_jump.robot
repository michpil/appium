*** Settings ***
Documentation  Ski Jump Automation
Library  AppiumLibrary
Library  dsj2_helpers.py


*** Variables ***
${DEVICE_NAME}                emulator-5554
#${DEVICE_NAME}                420064dfe42a9525
${ANDROID_PLATFORM_VERSION}   11.0
#${ANDROID_PLATFORM_VERSION}   9.0
${ANDROID_PLATFORM_NAME}      Android
${ANDROID_APP}                C:\\Users\\michp\\apks\\Deluxe Ski Jump 2.apk
${SERVER}                     http://localhost:4723/wd/hub
${DRIVER}                     UiAutomator2
${MODEL}                      Pixel_4


*** Test Cases ***
Ski Jump Test
  [Setup]  Test Startup
  [Teardown]  Test Shutdown

  Select Ads
  Continue to Game
  Select Practice
  Select Ski Jump
  Start
  Jump
  Fly
  Telemark No


*** Keywords ***
Test Startup
  clean screenshots

  Open Application  ${SERVER}
  ...  deviceName=${DEVICE_NAME}
  ...  platformName=${ANDROID_PLATFORM_NAME}
  ...  platformVersion=${ANDROID_PLATFORM_VERSION}
  ...  app=${ANDROID_APP}
  ...  automationName=${DRIVER}

  ${sid}=  get appium sessionid
  ${sid1}  ${scr_size}=  connect driver to session  ${sid}
  log  ${sid}
  log  ${sid1}
  log  ${scr_size}

  connect to device rotation  ${MODEL}

Select Ads
  Sleep  2
  ${x}  ${y}=  set scaled xy  ${0.4921962095875139}  ${0.6944444444444444}
  Click a point  ${x}  ${y}

Continue to Game
  Sleep  2
  ${x}  ${y}=  set scaled xy  ${0.2809364548494980}  ${0.8472222222222220}
  Click a point  ${x}  ${y}

Select Practice
  Sleep  2
  ${x}  ${y}=  set scaled xy  ${0.3069342251950950}  ${0.5231481481481480}
  Click a point  ${x}  ${y}

Select Ski Jump
  Sleep  2
  ${x}  ${y}=  set scaled xy  ${0.7088963210702340}  ${0.7848148148148150}
  Click a point  ${x}  ${y}

Start
  Sleep  2
#  Click a point  200  500
  run jumper
  Capture page screenshot  screen_start.png

Jump
  Sleep  4.4
  double touch  200  500  1700  500
  Capture page screenshot  screen_jump.png

Fly
#  Sleep  1
  rotate device manually  0  10
  Sleep  1
  Capture page screenshot  screen_rotate.png

Telemark No
  Sleep  1
  double touch  200  500  1700  500
  Capture page screenshot  screen_landing_tele_no.png

Telemark Yes
  Sleep  1
  Click a point  200  500
  Sleep  0.2
  Click a point  1700  500
  Capture page screenshot  screen_landing_tele_yes.png

Test Shutdown
  Sleep  6
  rotate device manually  0  -10
  Close application
