# from time import sleep
import requests
import os
from pywinauto.application import Application

from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction
from appium.webdriver.common.multi_action import MultiAction
from selenium.webdriver.common.action_chains import ActionChains


def connect_driver_to_session(s_id):
    desired_cap = {
      "platformName": "Android",
      "platformVersion": "11.0",
      "deviceName": "emulator-5554",
      "automationName": "UiAutomator2",
      "noReset": True
    }

    global driver
    driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_cap)

    global sid
    sid = s_id
    driver.session_id = sid

    global scr_size
    scr_size = driver.get_window_size()

    return sid, scr_size


def set_scaled_xy(x_ftr, y_ftr):
    wdh = scr_size['width']
    hgh = scr_size['height']
    x = int(wdh * x_ftr)
    y = int(hgh * y_ftr)

    return x, y


def close_session():
    driver.close()


def double_touch(x1, y1, x2, y2):

    user_action1 = TouchAction()
    user_action1.tap(x=int(x1), y=int(y1))

    user_action2 = TouchAction()
    user_action2.tap(x=int(x2), y=int(y2))

    ma = MultiAction(driver)
    ma.add(user_action1, user_action2)
    ma.perform()

# def wait_seconds(seconds):
#     sleep(int(seconds))

def run_jumper():
    url = f'http://localhost:4723/wd/hub/session/{sid}/touch/perform'
    myobj = {"actions":
                 [{"action": "press", "options": {"x": 200, "y": 500}},
                  {"action": "wait", "options": {"ms": 100}},
                  {"action": "release", "options": {}}]}

    requests.post(url, json=myobj)


def connect_to_device_rotation(model):
    global app
    app = Application().connect(title=f'Extended controls - {model}_API_30:5554')
    # app.Qt5QWindowIcon.move_window(0, 0)


def rotate_device_manually(x_rot, y_rot):

    x1 = 400
    x2 = x1 + int(x_rot)

    y1 = 200
    y2 = y1 + int(y_rot)

    app.Qt5QWindowIcon.press_mouse_input(coords=(x1, y1))
    app.Qt5QWindowIcon.release_mouse_input(coords=(x2, y2))


def clean_screenshots():
    dir_name = "."
    files = os.listdir(dir_name)

    for file in files:
        if file.endswith(".png"):
            os.remove(os.path.join(dir_name, file))


# def telemark(x1, y1, x2, y2, wait_sec=0.5):
#
#     user_action1 = TouchAction(driver)
#     user_action1.tap(x=int(x1), y=int(y1))
#     user_action1.perform()
#
#     sleep(wait_sec)
#
#     user_action2 = TouchAction(driver)
#     user_action2.tap(x=int(x2), y=int(y2))
#     user_action2.perform()
