*** Settings ***
Documentation  FlashScore Automation
Library  AppiumLibrary
#Library  RobotEyes
Library  fl_helpers.py


*** Variables ***
${DEVICE_NAME}                emulator-5554
#${DEVICE_NAME}                420064dfe42a9525
${ANDROID_PLATFORM_VERSION}   11.0
#${ANDROID_PLATFORM_VERSION}   9.0
${ANDROID_PLATFORM_NAME}      Android
${ANDROID_APP}                C:\\Users\\michp\\apks\\FlashScore.apk
${SERVER}                     http://localhost:4723/wd/hub
${DRIVER}                     UiAutomator2

# locators
${button_agree}=  eu.livesport.FlashScore_com:id/btnAgree
${title}=  eu.livesport.FlashScore_com:id/abTitle
${search_button}=  eu.livesport.FlashScore_com:id/searchButton
${search_field}=  eu.livesport.FlashScore_com:id/search_field
${first_country}=  /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ListView/android.view.ViewGroup[1]/android.widget.TextView[2]
${last_country}=  /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ListView/android.view.ViewGroup[5]/android.widget.TextView[2]


*** Test Cases ***
FlashScore Test
  [Setup]  Test Startup
  [Teardown]  Test Shutdown

  Check Title
  Search Team
  Check Team
  Go back


*** Keywords ***
Test Startup
  clean screenshots
  Open Application  ${SERVER}
  ...  deviceName=${DEVICE_NAME}
  ...  platformName=${ANDROID_PLATFORM_NAME}
  ...  platformVersion=${ANDROID_PLATFORM_VERSION}
  ...  app=${ANDROID_APP}
  ...  automationName=${DRIVER}

  Wait until page contains element  ${button_agree}
  Click element  ${button_agree}

  Wait until page contains  Continue
  Click text  Continue

  ${sid}=  get appium sessionid
  ${sid1}  ${scr_size}=  connect driver to session  ${sid}
  log  ${sid}
  log  ${sid1}
  log  ${scr_size}


Check Title
  Wait until page contains element  ${title}
  Element text should be  ${title}  FOOTBALL  NOT OKAY


Search Team
  Wait until page contains element  ${search_button}
  Click element  ${search_button}
  Wait until page contains element  ${search_field}
  Input text  ${search_field}  gdynia
  Hide keyboard

  ${x1}  ${y1}=  set scaled xy  ${0.462962962962963}  ${0.5574136008918618}
  ${x2}  ${y2}=  set scaled xy  ${0.462962962962963}  ${0}
  Swipe  ${x1}  ${y1}  ${x2}  ${y2}
  Capture page screenshot  screen_actual.png


Check Team
  Wait until page contains element  xpath=${first_country}
  Element text should be  xpath=${first_country}  Poland  NOT OKAY
  Element should be visible  xpath=${first_country}


Test Shutdown
  Close application
