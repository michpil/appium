from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction
from fl_helpers import clean_screenshots


# locators
button_agree = 'eu.livesport.FlashScore_com:id/btnAgree'
title = 'eu.livesport.FlashScore_com:id/abTitle'
search_button = 'eu.livesport.FlashScore_com:id/searchButton'
search_field = 'eu.livesport.FlashScore_com:id/search_field'
first_country = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ListView/android.view.ViewGroup[1]/android.widget.TextView[2]'
last_country = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ListView/android.view.ViewGroup[5]/android.widget.TextView[2]'


def open_app():
    desired_cap = {
      "platformName": "Android",
      "platformVersion": "11.0",
      "deviceName": "emulator-5554",
      "automationName": "UiAutomator2",
      "app": "C:\\Users\\michp\\apks\\FlashScore.apk"
    }

    global driver
    driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_cap)
    global scr_size
    scr_size = driver.get_window_size()

    return scr_size


def set_scaled_xy(x_ftr, y_ftr):
    wdh = scr_size['width']
    hgh = scr_size['height']
    x = int(wdh * x_ftr)
    y = int(hgh * y_ftr)

    return x, y


def startup():
    clean_screenshots()
    open_app()

    driver.implicitly_wait(3)
    driver.find_element_by_id(button_agree).click()
    driver.implicitly_wait(3)
    x, y = set_scaled_xy(0.5074074074074074, 0.6007246376811594)
    driver.tap([(x, y)])


def get_title():
    driver.implicitly_wait(3)
    title_text = driver.find_element_by_id(title).get_attribute("text")
    return title_text


def teardown():
    driver.close_app()


def search_team():
    driver.implicitly_wait(3)
    driver.find_element_by_id(search_button).click()
    driver.implicitly_wait(3)
    search_field_ = driver.find_element_by_id(search_field)
    driver.implicitly_wait(3)
    search_field_.click()
    driver.implicitly_wait(3)
    search_field_.send_keys("gdynia")
    driver.implicitly_wait(3)
    driver.hide_keyboard()
    driver.implicitly_wait(3)
    x1, y1 = set_scaled_xy(0.462962962962963, 0.5574136008918618)
    x2, y2 = set_scaled_xy(0.462962962962963, 0)
    driver.swipe(x1, y1, x2, y2)
    driver.save_screenshot('screen_actual.png')

    f_country = driver.find_element_by_xpath(first_country).get_attribute("text")

    return f_country


def test_flashscore():
    startup()

    title_text = get_title()
    assert title_text == "FOOTBALL", 'NOT OKAY'

    f_country = search_team()
    assert f_country == 'Poland', 'NOT OKAY'

    teardown()


if __name__ == "__main__":
    test_flashscore()
